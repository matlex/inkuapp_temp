# -*- coding: utf-8 -*-
from django import template
from django.utils import timezone
from django.utils.html import format_html
from datetime import datetime, timedelta
register = template.Library()


@register.filter()
def loan_reqest_remaining_time(loan_end_date):
    """
    Takes end_date of loan request and returns formatted string with remaining request time
    :param loan_end_date:
    :return:
    """
    try:
        remaining_time = loan_end_date - timezone.now()
        if remaining_time.days:
            return "{} days, {} hours, {} minutes remaining".format(
                remaining_time.days,
                remaining_time.seconds//3600,
                int(remaining_time.seconds/60 % 60)
            )
        elif remaining_time.seconds//3600:  # hours
            return "{} hour(s), {} minute(s) remaining".format(
                remaining_time.seconds//3600,
                int(remaining_time.seconds/60 % 60)
            )
        else:
            return "{} minutes remaining".format(int(remaining_time.seconds/60 % 60))
    except TypeError:
        return None
