from django.apps import AppConfig


class InkuAppConfig(AppConfig):
    name = 'inku_app'
