# README #
chay
Some explanations about using inkuwebapp project.

### Setting up project from scratch: ###

* Prepare postgres server
* Create new postgres database (e.g. 'inku_db') and prepare postgres user to use newly created DB
* Prepare and activate python virtual environment
* Pull the project out from bitbucket repostitory
* Install all required applications into virtual environment via "pip install -r requirements.txt" command
* Create "secrets" directory in inkuapp/inkuapp directory
* Copy "templates_secrets.py" into secrets directory created on previous step, and then 
rename to secrets.py
* [Generate a new secret key for project](http://www.miniwebtool.com/django-secret-key-generator/)
* Fill in all constants in secrets.py depending on your instance environment
* You also need to create a **local.py** file with some additional settings. You can copy from **remote.py** file.
* Run "python manage.py runserver 0.0.0.0:8000" for testing purposes
* Run "python db_init.py" command which will create an admin account with the following credentials:
username - admin@inku.io
password - 1qaz1qaz
* Open your_server_ip_address:8000 URL to see the project runs and works properly

### Admin Panel for tracking user signups: ###

* Open "your_project_domain/admin" URL - [http://inku.io/admin]
* You will see a welcome screen with username & password fields for admin user.
* Enter administration account credentials and press LogIn
* You will see few sections for each application of the project
* In "SignUp Profiles" section you will see all user profiles registered via signup web page.
* You can open each user record to see it's detailed profile data.

### Useful notes: ###
* When you're logged in as admin you'd like to test signup process but the system will redirect you while you're logged in.
So just open "your_server_ip_address/accounts/logout" [http://inku.io/accounts/logout/] URL and the system will log out you from current session and now you can access to singup page.
* When you pull a new code from repository or change settings.py you have to restart gunicorn server.
You can do that via CLI by simply run "sudo systemctl restart gunicorn.service" command. There is no need to restart ngnix.
* Also you can restart gunicorn server by running ./restartserver.sh file from project's directory.
* To activate virtual environment just run "workon inku_env"
* After updating CSS, JS or another static files you need to run "python manage.py collectstatic" command to update all static files. And after that you can restart gunicorn.