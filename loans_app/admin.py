from django.contrib import admin
from django.contrib.admin.decorators import register
from loans_app.models import BusinessSector, LoanRequest, InkuScore


@register(LoanRequest)
class LoanRequestAdmin(admin.ModelAdmin):
    list_display = ['borrower', 'loan_amount', 'purpose', 'request_status']
    readonly_fields = ['founding_statement', 'bank_statement', 'bar_coded_idd', 'picture_of_business']


admin.site.register(BusinessSector)
admin.site.register(InkuScore)
