from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^request_loan/$', views.RequestLoanView.as_view(), name='request_loan'),
]
