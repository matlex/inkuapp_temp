from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView
from django.utils.decorators import method_decorator

from inku_app.decorators import user_is_lender

from .forms import LenderDepositForm
from loans_app.models import LoanRequest

user_model = get_user_model()
decorators = [login_required, user_is_lender]


@login_required
@user_is_lender
def index(request):
    return render(request, 'lender_app/index.html')


@method_decorator(decorators, name='dispatch')  # Apply decorators to dispatch CBV method
class LenderDepositView(CreateView):
    template_name = 'lender_app/deposit.html'
    form_class = LenderDepositForm
    success_url = reverse_lazy('lender:index')

    # Pre-fill initial data for lender field
    def get_initial(self):
        lender = get_object_or_404(user_model, pk=self.request.user.pk)
        return {
            'lender': lender,
        }

    def form_valid(self, form):
        # Apply form's lender field based on current user
        form.instance.lender = self.request.user
        form.save()
        messages.success(self.request, 'Deposit Request Submitted')
        return super(LenderDepositView, self).form_valid(form)


class LenderMarketplaceView(ListView):
    template_name = 'lender_app/loan_marketplace.html'
    model = LoanRequest
    context_object_name = "loan_requests"
    queryset = LoanRequest.objects.exclude(request_status=0)


def lender_my_account_view(request):
    return render(request, 'lender_app/my_account.html')


class Investments(ListView):
    template_name = 'lender_app/investments.html'
    model = LoanRequest

