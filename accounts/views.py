from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib import messages

from .forms import BorrowerSignupForm, LenderSignupForm


# Helpers
def send_email_upon_registration(request):
    try:
        payload = {
            "host": request.get_host(),
        }
        html_message_to_send = render_to_string('inku_app/email/new_signup_notification.html', context=payload)
        txt_message_to_send = render_to_string('inku_app/email/new_signup_notification.txt', context=payload)
        send_mail(
            subject='New user has been registered at Inku.io',
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[settings.DEFAULT_TO_EMAIL],
            message=txt_message_to_send,
            html_message=html_message_to_send,
            fail_silently=False
        )
    except Exception as err:
        messages.error(request, err)


def signup_index_page(request):
    return render(request, 'account/index_page_signup.html')


def signup_borrower(request):
    if request.method == 'POST':
        form = BorrowerSignupForm(request.POST)
        if form.is_valid():
            form.save()

            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=raw_password)
            login(request, user)

            # Send out message to inku.io staff about new user registration.
            send_email_upon_registration(request)
            messages.success(request, "Successfully Registered")

            return redirect('borrower:index')
    else:
        form = BorrowerSignupForm()
    return render(request, "account/borrower_signup.html", {'form': form})


def signup_lender(request):
    if request.method == 'POST':
        form = LenderSignupForm(request.POST)
        if form.is_valid():
            form.save()

            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=email, password=raw_password)
            login(request, user)

            # Send out message to inku.io staff about new user registration.
            send_email_upon_registration(request)
            messages.success(request, "Successfully Registered")

            return redirect('lender:index')
    else:
        form = LenderSignupForm()
    return render(request, "account/lender_signup.html", {'form': form})


@login_required
def portal_choose(request):
    """
    Each logged in user will be redirected here and then this view will decide where redirect user to.
    :param request:
    :return:
    """
    if request.user.groups.first().name == "Borrower":
        return redirect('borrower:index')
    elif request.user.groups.first().name == "Lender":
        return redirect('lender:index')
    else:
        messages.error(request, "You successfully logged in, but something happened on our site. "
                                "Please contact with conor@inku.io to fix this problem.")
        return redirect('home')
