from django.db import models
from django.conf import settings

user_model = settings.AUTH_USER_MODEL


class LenderDeposit(models.Model):
    lender = models.ForeignKey(user_model, related_name='deposits', verbose_name='Lender email')
    bank_name = models.CharField(max_length=255)
    branch_code = models.CharField(max_length=50)
    account_number = models.CharField(max_length=50)
    fund_amount = models.PositiveIntegerField()

    def __str__(self):
        return self.lender.email
