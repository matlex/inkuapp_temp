from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/$', views.Loanpage.as_view(), name='loan_request_details')
]
