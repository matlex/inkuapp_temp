from datetime import timedelta

from django.utils import timezone
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from .models import LoanRequest


@receiver(pre_save, sender=LoanRequest)
def start_loan_request(sender, instance, **kwargs):
    """Updates end_at_date field depending on it's status"""
    # REQUEST_STATUSES = (
    #     (0, 'Pending'),
    #     (1, 'Approved'),
    #     (2, 'Declined'),
    # )
    if not instance.end_at_date and instance.request_status != 0:
        # instance.end_at_date = timezone.now() + timedelta(days=10)
        instance.end_at_date = timezone.now() + timedelta(minutes=120)
    elif instance.request_status == 0:
        instance.end_at_date = None


@receiver(post_save, sender=LoanRequest)
def initialize_loan_remaining_amount(sender, instance, created, **kwargs):
    if created:
        instance.remaining_amount = instance.loan_amount
