from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import user_passes_test
from . import views

login_forbidden = user_passes_test(lambda u: u.is_anonymous(), '/', redirect_field_name=None)

urlpatterns = [
    url('^signup_as/$', login_forbidden(views.signup_index_page), name='signup_index_page'),
    url('^signup_borrower/$', login_forbidden(views.signup_borrower), name='signup_borrower'),
    url('^signup_lender/$', login_forbidden(views.signup_lender), name='signup_lender'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^login/$', login_forbidden(auth_views.login), {'template_name': 'account/login.html'}, name='login'),
    url(r'^portal/$', views.portal_choose, name='portal'),
]
