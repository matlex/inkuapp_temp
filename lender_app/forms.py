from django import forms
from .models import LenderDeposit


class LenderDepositForm(forms.ModelForm):
    class Meta:
        model = LenderDeposit
        fields = '__all__'
        exclude = ['lender']
