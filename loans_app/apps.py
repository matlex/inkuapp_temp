from django.apps import AppConfig


class LoansAppConfig(AppConfig):
    name = 'loans_app'

    def ready(self):
        import loans_app.signals
