from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth import get_user_model

from inku_app.decorators import user_is_borrower
from .forms import RequestLoanForm

user_model = get_user_model()
decorators = [login_required, user_is_borrower]


@login_required
@user_is_borrower
def index(request):
    return render(request, 'borrower_app/index.html')


@method_decorator(decorators, name='dispatch')
class RequestLoanView(CreateView):
    template_name = 'borrower_app/request_loan.html'
    form_class = RequestLoanForm
    success_url = reverse_lazy('borrower:index')

    def form_valid(self, form):
        # Apply form's borrower field based on current user
        user = self.request.user
        form.instance.borrower = user
        form.save()
        messages.success(self.request, "Loan Request Submitted")
        return super(RequestLoanView, self).form_valid(form)

    # Pre-fill initial data for borrower field
    def get_initial(self):
        borrower = get_object_or_404(user_model, pk=self.request.user.pk)
        return {
            'borrower': borrower,
        }
