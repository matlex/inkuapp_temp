import os
import datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "inkuapp.settings")
import django
django.setup()
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from loans_app.models import BusinessSector

print("Initialization started")

try:
    # Lets create superuser
    admin = get_user_model()(
        # username='admin',
        email='admin@inku.io',
        is_active=True,
        is_staff=True,
        is_superuser=True,
        business_name="ITS",
        business_alias="ITS",
        address="Mailina 20",
        city="NY",
        business_registration_number="AAA111222333",
        business_number_employees=15,

        first_name='admin first name',
        last_name='admin last name',
        date_of_birth=datetime.datetime.now(),
        identity_number="ZZZ222333444",
        phone_number="+77051805545"
    )
    admin.set_password('1qaz1qaz')
    admin.save()
except Exception as e:
    pass

Group.objects.get_or_create(name="Borrower")
Group.objects.get_or_create(name="Lender")

business_sectors = ['Agricultural', 'Manufacturer', 'Wholesaler', 'Retailer', 'Services', 'Construction']
for i in business_sectors:
    BusinessSector(name=i).save()

print('Done!')
