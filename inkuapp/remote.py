from inkuapp.secrets.secrets import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = SECRET_KEY

DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}

EMAIL_HOST = SENDMAIL_HOST
EMAIL_PORT = "587"
EMAIL_USE_TLS = True
EMAIL_HOST_USER = SENDMAIL_HOST_USER
EMAIL_HOST_PASSWORD = SENDMAIL_PASSWORD
DEFAULT_FROM_EMAIL = SENDMAIL_DEFAULT_FROM_EMAIL
DEFAULT_TO_EMAIL = SENDMAIL_DEFAULT_TO_EMAIL
