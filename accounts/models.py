# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.core.mail import send_mail

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email address', max_length=150, unique=True)
    first_name = models.CharField('first name', max_length=30, blank=True)
    last_name = models.CharField('last name', max_length=30, blank=True)
    date_joined = models.DateTimeField('date joined', auto_now_add=True)
    is_active = models.BooleanField('active', default=True)
    is_staff = models.BooleanField('staff', default=False)

    business_name = models.CharField(verbose_name='Business name', max_length=128)
    business_alias = models.CharField(max_length=128)
    address = models.CharField(max_length=128)
    city = models.CharField(max_length=64)
    postal_code = models.CharField(max_length=12)

    business_registration_number = models.CharField(max_length=32, blank=True)
    business_time_in_business = models.PositiveIntegerField(null=True)
    business_annual_revenue = models.IntegerField(null=True)
    business_number_employees = models.IntegerField(null=True)

    date_of_birth = models.DateField(null=True)
    identity_number = models.CharField(max_length=32)
    phone_number = models.CharField(max_length=32)

    def get_account_type(self):
        return self.groups.first()

    get_account_type.short_description = 'Account type'
    get_account_type.allow_tags = True

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


# class User(AbstractUser):
#     business_name = models.CharField(verbose_name='Business name', max_length=128)
#     business_alias = models.CharField(max_length=128)
#     address = models.CharField(max_length=128)
#     city = models.CharField(max_length=64)
#     postal_code = models.CharField(max_length=12)
#
#     business_registration_number = models.CharField(max_length=32, blank=True)
#     business_time_in_business = models.PositiveIntegerField(null=True)
#     business_annual_revenue = models.IntegerField(null=True)
#     business_number_employees = models.IntegerField(null=True)
#
#     date_of_birth = models.DateField(null=True)
#     identity_number = models.CharField(max_length=32)
#     phone_number = models.CharField(max_length=32)
#
#     # email = models.EmailField(_('email address'), unique=True)
#
#     # USERNAME_FIELD = 'email'
#     # REQUIRED_FIELDS = []
#
#     def get_account_type(self):
#         return self.groups.first()
#
#     get_account_type.short_description = 'Account type'
#     get_account_type.allow_tags = True
