from django import forms
from loans_app.models import LoanRequest


class RequestLoanForm(forms.ModelForm):
    class Meta:
        model = LoanRequest
        fields = '__all__'
        # We don't want to show a user choice but we still need prefill borrower field in view
        exclude = ['borrower', 'request_status', 'started_at', 'remaining_amount', 'average_interest_rate', 'inku_score']
