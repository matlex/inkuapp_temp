from django.shortcuts import render


def index(request):
    return render(request, 'inku_app/index.html')
