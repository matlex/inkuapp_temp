"""
1) Create secrets directory
2) Copy and rename current file into secrets directory as secrets.py
3) Fill in necessary fields according your needs.
"""
SECRET_KEY = ''

DB_NAME = 'inku_db'
DB_USER = ''
DB_PASSWORD = ''
DB_HOST = ''
DB_PORT = '3306'

SENDMAIL_HOST = ''
SENDMAIL_HOST_USER = ''
SENDMAIL_PASSWORD = ''
SENDMAIL_DEFAULT_FROM_EMAIL = "Service clients Inku.io <noreply@inku.io>"
SENDMAIL_DEFAULT_TO_EMAIL = ''
