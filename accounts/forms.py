from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group


class BorrowerSignupForm(UserCreationForm):
    group_name = "Borrower"

    time_in_business_choices = [
        (0, 'How long are you in business?'),
        (1, '1 year'),
        (2, '2 years'),
        (3, '3 years'),
        (4, '4 years'),
        (5, '5 years'),
        (6, 'More than 5 years')
    ]

    business_name = forms.CharField(max_length=128)
    business_alias = forms.CharField(max_length=128)
    address = forms.CharField(max_length=128)
    city = forms.CharField(max_length=64)
    business_registration_number = forms.CharField(max_length=32)
    business_time_in_business = forms.ChoiceField(required=False, choices=time_in_business_choices)
    business_annual_revenue = forms.IntegerField(required=False)
    business_number_employees = forms.IntegerField()

    first_name = forms.CharField(max_length=32)
    last_name = forms.CharField(max_length=32)
    date_of_birth = forms.DateField()
    identity_number = forms.CharField(max_length=32)
    phone_number = forms.CharField(max_length=34)

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name', 'date_of_birth', 'identity_number', 'phone_number',
                  'business_name', 'business_alias', 'address', 'city', 'business_registration_number',
                  'business_annual_revenue', 'business_time_in_business', 'business_number_employees',
                  )

    def save(self, commit=True):
        user = super(BorrowerSignupForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            # Add user to group_name
            user.groups.add(Group.objects.get_or_create(name=self.group_name)[0])
            user.save()
        return user


class LenderSignupForm(UserCreationForm):
    group_name = "Lender"

    business_name = forms.CharField(max_length=128)
    business_alias = forms.CharField(max_length=128)
    address = forms.CharField(max_length=128)
    city = forms.CharField(max_length=64)
    postal_code = forms.CharField(max_length=12)

    first_name = forms.CharField(max_length=32)
    last_name = forms.CharField(max_length=32)
    date_of_birth = forms.DateField()
    identity_number = forms.CharField(max_length=32)
    phone_number = forms.CharField(max_length=34)

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name', 'date_of_birth', 'identity_number', 'phone_number',
                  'business_name', 'business_alias', 'address', 'city', 'postal_code',)

    def save(self, commit=True):
        user = super(LenderSignupForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
            # Add user to group_name
            user.groups.add(Group.objects.get_or_create(name=self.group_name)[0])
            user.save()
        return user
