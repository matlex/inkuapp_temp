from django.db import models
from django.conf import settings

user_model = settings.AUTH_USER_MODEL


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/loan_requests/user_<id>/<filename>
    return 'loan_requests/user_{0}/{1}/'.format(instance.borrower.email, filename)


class BusinessSector(models.Model):
    name = models.CharField(max_length=60, unique=True)

    def __str__(self):
        return self.name


class InkuScore(models.Model):
    title = models.CharField(max_length=5)

    def __str__(self):
        return self.title


class LoanRequest(models.Model):
    REQUEST_STATUSES = (
        (0, 'Pending'),
        (1, 'Approved'),
        (2, 'Declined'),
    )
    borrower = models.ForeignKey(user_model, related_name='loan_requests', verbose_name='Borrower email')
    business_sector = models.ForeignKey(BusinessSector, related_name='loan_request')

    loan_amount = models.PositiveIntegerField()
    term = models.PositiveIntegerField()
    purpose = models.TextField(max_length=175)
    bridge_finance = models.BooleanField(default=False)  # Bridge Finance? (AKA contract already in place?)

    bank_name = models.CharField(max_length=255)
    branch_code = models.CharField(max_length=50)
    account_number = models.CharField(max_length=50)

    founding_statement = models.FileField(upload_to=user_directory_path)  # Recent version of the founding statement and certificate of Incorporation
    bank_statement = models.FileField(upload_to=user_directory_path)  # Utility Bill, Bank statement, or telephone bill showing address
    bar_coded_idd = models.FileField(upload_to=user_directory_path)  # Green Bar-coded identity document

    request_status = models.IntegerField(choices=REQUEST_STATUSES, default=0, verbose_name='Loan request status')
    applied_at_date = models.DateTimeField(auto_now=True)
    end_at_date = models.DateTimeField(null=True, blank=True)  # There will be countdown from 1 month on frontend.

    picture_of_business = models.ImageField(upload_to=user_directory_path, default='misc/Shop.png')
    black_ownership_percent = models.PositiveIntegerField()

    remaining_amount = models.PositiveIntegerField(default=0)
    average_interest_rate = models.FloatField(default=0)
    inku_score = models.ForeignKey(InkuScore, null=True)

    def __str__(self):
        return self.borrower.email
