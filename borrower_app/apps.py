from django.apps import AppConfig


class BorrowerAppConfig(AppConfig):
    name = 'borrower_app'
