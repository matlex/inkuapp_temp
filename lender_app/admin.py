from django.contrib import admin
from django.contrib.admin.decorators import register

from.models import LenderDeposit


@register(LenderDeposit)
class LenderDepositRequest(admin.ModelAdmin):
    list_display = ['lender', 'fund_amount']
    readonly_fields = ['fund_amount']
