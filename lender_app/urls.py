from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^deposit/$', views.LenderDepositView.as_view(), name='deposit_loan'),
    url(r'^marketplace/$', views.LenderMarketplaceView.as_view(), name='loan_marketplace'),
    url(r'^my_account/$', views.lender_my_account_view, name='lender_my_account'),
    url(r'^investments/$', views.Investments.as_view(), name='investments'),
]
