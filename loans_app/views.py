from django.shortcuts import render
from django.views.generic.detail import DetailView

from .models import LoanRequest


class Loanpage(DetailView):
    template_name = 'loans_app/loanpage.html'
    model = LoanRequest
