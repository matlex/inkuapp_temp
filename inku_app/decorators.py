from django.contrib.auth.decorators import user_passes_test


user_is_borrower = user_passes_test(lambda user: user.groups.first().name == "Borrower", 'lender:index', redirect_field_name=None)
user_is_lender = user_passes_test(lambda user: user.groups.first().name == "Lender", 'borrower:index', redirect_field_name=None)

