# -*- coding: utf-8 -*-

from django.contrib import admin
from django.utils.translation import ugettext as _
from django.contrib.admin.decorators import register
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin


@register(get_user_model())
class CustomUserAdmin(UserAdmin):

    list_display = ('email', 'first_name', 'last_name', 'date_joined', 'get_account_type')
    ordering = ('email',)

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)

    fieldsets = (
        (_('Personal info'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ('get_account_type', 'first_name', 'last_name', 'email', 'date_of_birth',
                       'identity_number', 'phone_number')
        }),
        ('Business info', {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ('business_name', 'business_alias', 'address', 'city', 'postal_code',
                       'business_registration_number', 'business_time_in_business', 'business_annual_revenue',
                       'business_number_employees',)
        }),
        (_('Readonly'), {
            'classes': ('suit-tab', 'suit-tab-advanced',),
            'fields': ('password',)
        }),
        (_('Permissions'), {
            'classes': ('suit-tab', 'suit-tab-advanced',),
            'fields': ('is_active', 'is_staff', 'is_superuser',
                       'groups', 'user_permissions')
        }),
        (_('Important dates'), {
            'classes': ('suit-tab', 'suit-tab-advanced',),
            'fields': ('last_login', 'date_joined')
        }),
    )
    readonly_fields = ('get_account_type', 'date_joined')

    # (TAB_NAME, TAB_TITLE)
    suit_form_tabs = (('general', 'General'), ('advanced', 'Advanced Settings'))
